import {StyleSheet, Text, View, FlatList, SafeAreaView, Image } from "react-native";

export default function App() {
  const animales = [
    {
      id: 1,
      name: '8-Agosto-2022, mycam.jpg',
      image: require('./assets/img/imagen1.jpg'),
    },
    {
      id: 2,
      name: '20-Julio-2022, mycam.jpg',
      image: require('./assets/img/imagen2.jpg'),
    },
    {
      id: 3,
      name: '6-Agosto-2022, mycam.jpg',
      image: require('./assets/img/imagen3.jpg'),
    },
    {
      id:4,
      name: '9-Agosto-2022, mycam.jpg',
      image: require('./assets/img/imagen4.jpg'),
    },
    {
      id:5,
      name: '15-Agosto-2022, mycam.jpg',
      image: require('./assets/img/imagen5.jpg'),
    },
  ];

  const oneAnimal = ( {item} ) => (

    <View style={styles.item}>
      <View style={styles.avatarContainer}>
        <Image source={item.image} style={styles.avatar} />
      </View>
      <Text style={styles.name}>{item.name}</Text>
    </View>
  )

  headerComponent = () => {
    return <Text style={styles.listHeadLine}>Fotofrafías</Text>
  }

  itemSeparator = () => {
    return <View style={styles.separator} />
  }

  return (
    <SafeAreaView>
      <FlatList
        ListHeaderComponentStyle={styles.listHeader}
        ListHeaderComponent={headerComponent}
        data = { animales }
        renderItem = { oneAnimal }
        ItemSeparatorComponent = { itemSeparator }
      />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  listHeader: {
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listHeadLine: {
    color: '#333',
    fontSize: 21,
    fontWeight: 'bold',
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13,
  },

  avatar: {
    height: 110,
    width: 180,
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#CCC'
  }
});
