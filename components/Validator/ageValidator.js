import React from 'react';
import {View, Text, TextInput} from 'react-native';


const ageValidator = props => (
  <View>
      <TextInput keyboardType='numeric'
        style={{
          height: 40,
          borderColor: "gray",
          borderWidth: 1,
        }}
      />
      <Text>(age>=18)?'Es mayor de edad':'Es menor de edad'</Text>
  </View>
);

export default ageValidator;